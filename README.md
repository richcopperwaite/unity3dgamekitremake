# Unity Scene Importer for Unreal

_Featuring a scene recreated from Unity's free __3D Game Kit__ sample._

(https://assetstore.unity.com/packages/templates/tutorials/3d-game-kit-115747)

## Overview

This tool was created as part of a personal exploration into Unreal, with the goal of recreating a Unity sample project as quickly and accurately as possible. The tool was able to accelerate this process by placing thousands of individual objects in the exact same locations in the new scene, taking into account the differences in Unreal's coordinate space, unit-scale, and rotational handedness.

The Unity Scene Importer consists of:

1. A Unity C# Script to export game object transforms in a scene to an intermediate JSON file format.

_( Unityscripts / SceneToJSON.cs )_

2. An Unreal C++ plugin to read those JSON files and spawn corresponding static mesh actors in the scene.

_( Plugins / UnityLevelImporter / * )_

The sample project also contains an instantiable Unreal material that mimics the behaviour of Unity's Standard Shader.

_( Content / ThirdPersonCPP / OriginalContent / UnityStandard.uasset )_

__BE WARNED__ that these tools contain serious limitations that may require significant modifications by a skilled programmer to work for your project, for instance:

- The Unity export script will export transforms for all objects in the scene with the names you specify, regardless of whether they are compatible mesh or prefab instances. Ensure that your spelling is correct, and beware of undesired objects with matching names.

- The Unreal import plugin will only spawn simple actors from Static Mesh assets with the same names as objects in the JSON scene file. For your project, it may be preferable to alter the plugin to spawn more complex Actor Blueprints.

- Static Meshes corresponding to the specified Unity objects must be imported into your Unreal project manually before importing a JSON scene file. The tool will not migrate static mesh assets or their materials into your project for you.

- The math operations to convert Unity object rotations to Unreal space were devised by trial-and-error. I have tested them thoroughly enough to be confident that they work 100% of the time... but I'm not entirely sure _why_.

## Getting Started

1. Copy _UnityScripts/SceneToJSON.cs_ to the Assets directory of your Unity project (preferably to an Editor subfolder). Copy _Plugins/UnrealLevelImporter_ to the Plugins directory of your Unreal project (if it doesn't exist, create it).

2. In the Unity Assets panel, click 'Create', and choose 'SceneToJSON'. This will create a Scriptable Object containing options for your scene export.

3. Select the Scriptable Object you have just created, and in the Details panel, fill the list of Object Names with names of prefabs whose instances you wish to be exported. If the mesh name is different from the prefab name, open the Mesh Name Overrides list and enter the correct mesh name into the corresponding index - leave all other entries of this list blank.

4. Open the Unity scene you wish to export.

5. Select the Scriptable Object again, and in the Details panel, click the small gear icon in the top-right, and choose _'Save Scene to JSON'_.

6. Choose an appropriate name for your JSON export file, and click 'Save'. The tool will generate a JSON file containing names and transforms of all the prefab instances you have previously indicated.

7. Open your Unreal project, and for each of the above prefabs, import the associated static mesh assets. Feel free to use the UnityStandard shader from this sample project. 

8. In the Unreal toolbar, click the furthest-right icon, which should read _"Import level contents from Unity using a JSON interop file"_. Open the JSON file that you just exported from Unity.

9. After a few moments, you should see all your specified mesh actors spawned into the scene at their correct positions, scales and rotations.

