// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class Unity3DGameKitRemakeEditorTarget : TargetRules
{
	public Unity3DGameKitRemakeEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		ExtraModuleNames.Add("Unity3DGameKitRemake");
	}
}
