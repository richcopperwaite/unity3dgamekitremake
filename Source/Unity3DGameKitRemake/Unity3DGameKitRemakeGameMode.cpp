// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Unity3DGameKitRemakeGameMode.h"
#include "Unity3DGameKitRemakeCharacter.h"
#include "UObject/ConstructorHelpers.h"

AUnity3DGameKitRemakeGameMode::AUnity3DGameKitRemakeGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
