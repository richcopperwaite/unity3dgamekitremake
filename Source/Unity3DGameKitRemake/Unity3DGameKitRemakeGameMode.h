// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Unity3DGameKitRemakeGameMode.generated.h"

UCLASS(minimalapi)
class AUnity3DGameKitRemakeGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AUnity3DGameKitRemakeGameMode();
};



