// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class Unity3DGameKitRemakeTarget : TargetRules
{
	public Unity3DGameKitRemakeTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("Unity3DGameKitRemake");
	}
}
