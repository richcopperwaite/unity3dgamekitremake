#pragma once

#include "CoreMinimal.h"

#include "Modules/ModuleInterface.h"
#include "Modules/ModuleManager.h"
#include "UnrealEd.h"

DECLARE_LOG_CATEGORY_EXTERN(LogUnityLevelImporter, All, All)

class FUnityLevelImporterModule: public IModuleInterface
{
	TSharedPtr<FUICommandList> PluginCommands;
	TSharedPtr<FExtender> ToolbarExtender;

	void AddToolbarButton(FToolBarBuilder& Builder);
	void TestAction();
	void PlacingStaticMesh(UStaticMesh* myStaticMesh, FString name, FVector position, FRotator rotation, FVector scale);

public:
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;

};