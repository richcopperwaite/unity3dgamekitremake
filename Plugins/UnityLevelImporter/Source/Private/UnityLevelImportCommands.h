#pragma once

#include "Commands.h"
#include "EditorStyleSet.h"

class FUnityLevelImportCommands : public TCommands<FUnityLevelImportCommands>
{
public: 
	FUnityLevelImportCommands() : TCommands<FUnityLevelImportCommands>(
		TEXT("UnityLevelImporter"),
		NSLOCTEXT("Contexts", "DemoEditor", "Demo Editor"),
		NAME_None,
		FEditorStyle::GetStyleSetName()
		)
	{

	}

	virtual void RegisterCommands() override;

	TSharedPtr<FUICommandInfo> TestCommand;
};