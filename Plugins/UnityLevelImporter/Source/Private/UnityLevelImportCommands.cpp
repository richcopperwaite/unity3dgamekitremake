#include "UnityLevelImportCommands.h"

#define LOCTEXT_NAMESPACE "UnityLevelImporter"

void FUnityLevelImportCommands::RegisterCommands()
{
	UI_COMMAND(TestCommand, "Unity Import", "Import level contents from Unity using a JSON interop file", EUserInterfaceActionType::Button, FInputGesture());
}

#undef LOCTEXT_NAMESPACE