#include "UnityLevelImporter.h"
#include "Modules/ModuleManager.h"
#include "Modules/ModuleInterface.h"
#include "UnityLevelImportCommands.h"
#include "LevelEditor.h"
#include "Developer/DesktopPlatform/Public/DesktopPlatformModule.h"
#include "Editor.h"
#include "AssetRegistryModule.h"

IMPLEMENT_GAME_MODULE(FUnityLevelImporterModule, UnityLevelImporter);

DEFINE_LOG_CATEGORY(LogUnityLevelImporter)

#define LOCTEXT_NAMESPACE "UnityLevelImporter"

void FUnityLevelImporterModule::StartupModule()
{
	UE_LOG(LogUnityLevelImporter, Warning, TEXT("UnityLevelImporter: Log Started"));

	FUnityLevelImportCommands::Register();

	PluginCommands = MakeShareable(new FUICommandList);
	PluginCommands->MapAction(FUnityLevelImportCommands::Get().TestCommand,
		FExecuteAction::CreateRaw(this, &FUnityLevelImporterModule::TestAction));

	FLevelEditorModule& LevelEditorModule = FModuleManager::LoadModuleChecked<FLevelEditorModule>("LevelEditor");

	ToolbarExtender = MakeShareable(new FExtender);
	//TSharedRef<FExtender> ToolbarExtender(new FExtender());
	ToolbarExtender->AddToolBarExtension("Game", EExtensionHook::After, PluginCommands, FToolBarExtensionDelegate::CreateRaw(this, &FUnityLevelImporterModule::AddToolbarButton));

	LevelEditorModule.GetToolBarExtensibilityManager()->AddExtender(ToolbarExtender);
}

void FUnityLevelImporterModule::ShutdownModule()
{
	UE_LOG(LogUnityLevelImporter, Warning, TEXT("UnityLevelImporter: Log Ended"));

	FUnityLevelImportCommands::Unregister();

	FLevelEditorModule& LevelEditorModule = FModuleManager::LoadModuleChecked<FLevelEditorModule>("LevelEditor");

	LevelEditorModule.GetToolBarExtensibilityManager()->RemoveExtender(ToolbarExtender);
}

void FUnityLevelImporterModule::TestAction()
{
	UE_LOG(LogUnityLevelImporter, Warning, TEXT("It works"));

	TArray<FString> selectedFiles;

	FDesktopPlatformModule::Get()->OpenFileDialog(0,
		TEXT("Scene Interop File"),
		TEXT(""),
		TEXT(""),
		TEXT("JSON File|*.json"),
		0,
		selectedFiles);

	if (selectedFiles.Num() > 0)
	{
		UE_LOG(LogUnityLevelImporter, Warning, TEXT("%s"), *selectedFiles[0]);

		FString fileContent;

		if (FFileHelper::LoadFileToString(fileContent, *selectedFiles[0]))
		{
			TSharedRef<TJsonReader<TCHAR>> JsonReader = TJsonReaderFactory<TCHAR>::Create(fileContent);
			TArray<TSharedPtr<FJsonValue>> JsonParsed;
			if (FJsonSerializer::Deserialize(JsonReader, JsonParsed))
			{
				UE_LOG(LogUnityLevelImporter, Warning, TEXT("Number of objects: %d"), JsonParsed.Num());

				FAssetRegistryModule& AssetRegistryModule =
					FModuleManager::LoadModuleChecked<FAssetRegistryModule>("AssetRegistry");

				TArray<FAssetData> MeshAssetList;
				TMap<FName, FAssetData> AssetLookup;

				FARFilter filter;
				filter.ClassNames.Add(UStaticMesh::StaticClass()->GetFName());
				filter.PackagePaths.Add("/Game/ThirdPersonCPP/OriginalContent/Models");
				filter.bRecursivePaths = true;

				AssetRegistryModule.Get().GetAssets(filter, MeshAssetList);

				//for (int n = 0; n < MeshAssetList.Num(); ++n)
				//{
				//	UStaticMesh* staticMesh = (UStaticMesh*)MeshAssetList[n].GetAsset();
				//	int32 numLods = staticMesh->GetNumLODs();
				//	if (numLods > 0)
				//	{
				//		UE_LOG(MyGameEditor, Warning, TEXT("Model %s has %d lods!"), *staticMesh->GetName(), numLods);
				//	}
				//}

				for (int i = 0; i < JsonParsed.Num(); ++i)
				{
					FString assetString;
					TSharedPtr<FJsonObject> jsonObject = JsonParsed[i]->AsObject();
					jsonObject->TryGetStringField(TEXT("name"), assetString);
					FName assetName(*assetString);

					UStaticMesh* staticMesh = 0;

					if (AssetLookup.Contains(assetName))
					{
						staticMesh = (UStaticMesh*) AssetLookup[assetName].GetAsset();
					}
					else
					{
						for (int m = 0; m < MeshAssetList.Num(); ++m)
						{
							if (MeshAssetList[m].AssetName.ToString().EndsWith(assetString))
							{
								AssetLookup.Add(assetName, MeshAssetList[m]);

								staticMesh = (UStaticMesh*) MeshAssetList[m].GetAsset();
							}
						}
					}

					if (staticMesh == 0)
						continue;

					double PosX, PosY, PosZ, /*RotX, RotY, RotZ,*/ ScaX, ScaY, ScaZ;
					double QuatX, QuatY, QuatZ, QuatW;

					jsonObject->TryGetNumberField("PosX", PosX);
					jsonObject->TryGetNumberField("PosY", PosY);
					jsonObject->TryGetNumberField("PosZ", PosZ);
					//jsonObject->TryGetNumberField("RotX", RotX);
					//jsonObject->TryGetNumberField("RotY", RotY);
					//jsonObject->TryGetNumberField("RotZ", RotZ);
					jsonObject->TryGetNumberField("QuatX", QuatX);
					jsonObject->TryGetNumberField("QuatY", QuatY);
					jsonObject->TryGetNumberField("QuatZ", QuatZ);
					jsonObject->TryGetNumberField("QuatW", QuatW);
					jsonObject->TryGetNumberField("ScaX", ScaX);
					jsonObject->TryGetNumberField("ScaY", ScaY);
					jsonObject->TryGetNumberField("ScaZ", ScaZ);

					FVector position(PosX * -100.0, PosZ * 100.0, PosY * 100.0);
					//FRotator rotation(0.0f, RotY, 0.0f);
					//rotation.Add(0.0f, 0.0f, RotX);
					//rotation.Add(-RotZ, 0.0f, 0.0f);

					// https://wiki.unrealengine.com/Transitioning_from_Unity_to_UE4#Quaternion_Conversions
					//FQuat q = FQuat(-QuatX, -QuatZ, -QuatY, QuatW); // THIS IS REALLY CLOSE, JUST UPSIDE-DOWN
					FQuat q = FQuat(-QuatX, -QuatZ, -QuatY, QuatW);

					// NEED TO FLIP 180

					FQuat flip = FQuat::MakeFromEuler(FVector(0.0f, 180.0f, 0.0f));
					FQuat flip2 = FQuat::MakeFromEuler(FVector(0.0f, 0.0f, 180.0f));
					FQuat flip3 = FQuat::MakeFromEuler(FVector(180.0f, 0.0f, 0.0f));

					//FVector axis;
					//float angle;
					//q.ToAxisAndAngle(axis, angle);
					//axis = FVector(axis.Z, -axis.X, axis.Y);
					//FQuat q2 = FQuat(FVector::UpVector, FMath::DegreesToRadians(90.0f)) * FQuat(axis, -angle);

					FRotator rotation(flip * flip2 * q * flip3);
					FVector scale(ScaX, ScaZ, ScaY);

					PlacingStaticMesh(staticMesh, assetString, position, rotation, scale);
				}
			}
		}
	}
}

void FUnityLevelImporterModule::PlacingStaticMesh(UStaticMesh* myStaticMesh, FString name, FVector position, FRotator rotation, FVector scale) {
	// Name & Transform
	//FVector objectPosition(0, 0, 0);
	//FRotator objectRotation(0, 0, 0); //in degrees
	//FVector objectScale(1, 1, 1);
	FTransform objectTrasform(rotation, position, scale);

	// Creating the Actor and Positioning it in the World based in the Static Mesh
	UWorld * currentWorld = GEditor->GetEditorWorldContext().World();
	ULevel * currentLevel = currentWorld->GetCurrentLevel();
	UClass * staticMeshClass = AStaticMeshActor::StaticClass();

	AActor * newActorCreated = GEditor->AddActor(currentLevel, staticMeshClass, objectTrasform, true, RF_Public | RF_Standalone | RF_Transactional);

	AStaticMeshActor * smActor = Cast<AStaticMeshActor>(newActorCreated);

	smActor->GetStaticMeshComponent()->SetStaticMesh(myStaticMesh);
	smActor->SetActorScale3D(scale);

	int actorIndex = 0;
	FString actorName = FString::Printf(TEXT("%s_%d"), *name, actorIndex);
	while (!smActor->Rename(*actorName, nullptr, REN_Test))
	{
		actorIndex++;
		actorName = FString::Printf(TEXT("%s_%d"), *name, actorIndex);
	}
	smActor->Rename(*actorName); 
	smActor->SetActorLabel(*actorName);

	GEditor->EditorUpdateComponents();
	smActor->GetStaticMeshComponent()->RegisterComponentWithWorld(currentWorld);
	currentWorld->UpdateWorldComponents(true, false);
	smActor->RerunConstructionScripts();
	GLevelEditorModeTools().MapChangeNotify();
}

void FUnityLevelImporterModule::AddToolbarButton(FToolBarBuilder& Builder)
{
	FSlateIcon IconBrush = FSlateIcon(FEditorStyle::GetStyleSetName(), "LevelEditor.PlacementMode", "LevelEditor.PlacementMode.Small");

	Builder.AddToolBarButton(FUnityLevelImportCommands::Get().TestCommand, NAME_None, FText::FromString("Unity Import"), FText::FromString("Import level contents from Unity using a JSON interop file"), IconBrush, NAME_None);
}

#undef LOCTEXT_NAMESPACE