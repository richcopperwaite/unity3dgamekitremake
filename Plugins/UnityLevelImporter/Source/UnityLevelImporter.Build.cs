// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class UnityLevelImporter : ModuleRules
{
	public UnityLevelImporter(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
        PublicIncludePaths.AddRange(
            new string[]
            {
            });

        PrivateIncludePaths.AddRange(
            new string[] 
            {
                "Private"
            });

        PublicDependencyModuleNames.AddRange(
            new string[]
            {
                "Core",
                "CoreUObject",
                "Engine",
                "InputCore",
                "Unity3DGameKitRemake"
            });
        PrivateDependencyModuleNames.AddRange(
            new string[] 
            {
                "UnrealEd",
                "DesktopPlatform",
                "LevelEditor",
                "Slate",
                "SlateCore",
                "EditorStyle",
                "Json",
                "AssetRegistry"
            });

        PrivateIncludePathModuleNames.AddRange(
            new string[]
            {
            });

        DynamicallyLoadedModuleNames.AddRange(
            new string[] 
            {
            });

    }

}
