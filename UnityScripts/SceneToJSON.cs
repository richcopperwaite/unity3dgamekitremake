﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

[CreateAssetMenu(fileName ="SceneToJson.asset", menuName ="Scene To JSON")]
public class SceneToJSON : ScriptableObject
{
    public string[] objectNames = new string[0];

    public string[] meshNameOverrides = new string[0];

    [System.Serializable]
    private class ObjectJsonRepresentation
    {
        public string name;
        public float PosX;
        public float PosY;
        public float PosZ;
        public float QuatX;
        public float QuatY;
        public float QuatZ;
        public float QuatW;
        public float ScaX;
        public float ScaY;
        public float ScaZ;
    }

    [ContextMenu("Save Scene To JSON")]
    public void SaveSceneToJSON()
    {
        string filePath = EditorUtility.SaveFilePanel("Save Scene JSON", "/", "Scene01.json", "json");
        
        List<ObjectJsonRepresentation> objectList = new List<ObjectJsonRepresentation>();

        string sceneRepresentation = "[";

        bool first = true;

        for (int i = 0; i < objectNames.Length; ++i)
        {
            string objectName = objectNames[i];

            GameObject[] gameObjects = Resources.FindObjectsOfTypeAll<GameObject>().Where(obj => obj.name.Equals(objectName) && obj.activeInHierarchy == true).ToArray();

            int duplicateIndex = 1;
            string name = string.Format("{0} ({1})", objectName, duplicateIndex);
            GameObject[] duplicates = Resources.FindObjectsOfTypeAll<GameObject>().Where(obj => obj.name.Equals(name) && obj.activeInHierarchy == true).ToArray();
            while(duplicates != null & duplicates.Length > 0)
            {
                ArrayUtility.AddRange(ref gameObjects, duplicates);
                ++duplicateIndex;
                name = string.Format("{0} ({1})", objectName, duplicateIndex);
                duplicates = Resources.FindObjectsOfTypeAll<GameObject>().Where(obj => obj.name.Equals(name) && obj.activeInHierarchy == true).ToArray();
            }

            if(i < meshNameOverrides.Length && !string.IsNullOrEmpty(meshNameOverrides[i]))
            {
                objectName = meshNameOverrides[i];
            }

            foreach (GameObject go in gameObjects)
            {
                Vector3 eulerAngles = go.transform.rotation.eulerAngles;
                ObjectJsonRepresentation rep = new ObjectJsonRepresentation()
                {
                    name =  objectName,
                    PosX = go.transform.position.x,
                    PosY = go.transform.position.y,
                    PosZ = go.transform.position.z,
                    QuatX = go.transform.rotation.x,
                    QuatY = go.transform.rotation.y,
                    QuatZ = go.transform.rotation.z,
                    QuatW = go.transform.rotation.w,
                    ScaX = go.transform.lossyScale.x,
                    ScaY = go.transform.lossyScale.y,
                    ScaZ = go.transform.lossyScale.z
                };

                if (!first)
                    sceneRepresentation += "\n,";
                first = false;
                    
                sceneRepresentation += JsonUtility.ToJson(rep, true);
            }
        }

        sceneRepresentation += "]";

        System.IO.File.WriteAllText(filePath, sceneRepresentation);
    }
}
